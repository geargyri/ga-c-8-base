# CentOS 8 Docker images

http://cern.ch/linux/centos8/

## What is CentOS 8 (C8) ?

Upstream CentOS 8, with minimal patches to integrate into the CERN computing environment

### Image building

```
koji image-build c8-docker-base `date "+%Y%m%d"` c8-image-8x \
	http://linuxsoft.cern.ch/cern/centos/8/BaseOS/\$arch/ x86_64 aarch64 \
	--ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/c8-base#master \
	--kickstart=c8-base-docker.ks --ksversion=RHEL8 --distro=RHEL-8.2 --format=docker \
	--factory-parameter=dockerversion 1.10.1 \
	--factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	--factory-parameter=docker_cmd '["/bin/bash"]' \
	--factory-parameter=generate_icicle False \
	--wait --scratch
```

### Latest image
